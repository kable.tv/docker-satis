FROM composer/satis
RUN apk add --update openssh-client fuse git ca-certificates

#Add GSCFUSE for Object storage
ADD linux_amd64/gcsfuse /usr/local/bin

EXPOSE 80
WORKDIR /build
CMD ["php", "/satis/bin/satis", "build", "-n"]
