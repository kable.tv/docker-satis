This image is a composer/satis image which also provide gcsfuse binary so that
it's possible to mount a GCS Bucket
### How to
1. Mount your ssh keys to /root/.ssh
2. Mount your build dir to /build (where the statis.json file is)
3. Run the satis build command with `php /satis/bin/satis build -n`
4. Bob's your uncle
